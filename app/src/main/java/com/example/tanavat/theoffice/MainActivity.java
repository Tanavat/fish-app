package com.example.tanavat.theoffice;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText editSearch;
    private ImageButton btnSearch;
    private ListView productListview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //      Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();

                Intent NewProductActivity = new Intent(getApplicationContext(),NewProductActivity.class);
                startActivity(NewProductActivity);

            }
        });


        productListview = (ListView)findViewById(R.id.product_listview);

        //ArrayList<Product> products =new ArrayList<>();
        // Product product = new Product();
        // product.setProductId("001");
        // product.setNameProduct("test");
        // product.setPrice(10);

        // products.add(product);

        // ProductListAdapter adapter = new ProductListAdapter(this,products);



    }

    @Override
    protected void onResume() {
        super.onResume();

        ProductDAO productDAO = new ProductDAO(this);

        final ProductListAdapter adapter = new ProductListAdapter(this,productDAO.findAll());
        productListview.setAdapter(adapter);
        productDAO.close();

        productListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent sellIntent = new Intent(MainActivity.this,SellActivity.class);
                sellIntent.putExtra("productObj",adapter.getItem(position));
                startActivity(sellIntent);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}