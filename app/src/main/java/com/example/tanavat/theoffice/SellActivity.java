package com.example.tanavat.theoffice;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SellActivity extends AppCompatActivity {
    private TextView sellName,typeName,priceSell,Data;
    private Button btnBack,btnDelete,btnEdit;
    private ImageView sellImage;
    private  Product product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell);

        sellName = (TextView)findViewById(R.id.sell_name);
        typeName = (TextView)findViewById(R.id.type_name);
        priceSell = (TextView)findViewById(R.id.price_sell);
        Data      = (TextView)findViewById(R.id.data_sell);


        Intent intent =getIntent();
     //   long productId = intent.getLongExtra("productId",0);
       product = (Product) intent.getSerializableExtra("productObj");

       sellName .setText(product.getNameProduct());
        typeName.setText(product.getTypeId());
        priceSell.setText(String.valueOf(product.getPrice()));
        Data.setText(product.getData());

        btnBack = (Button)findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent mainProduct = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(mainProduct);
            }
        });

        btnDelete = (Button)findViewById(R.id.product_delete_button);

        btnEdit = (Button)findViewById(R.id.product_edit_button);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editProductIntent = new Intent(getApplicationContext(),ProductEditActivity.class);
                editProductIntent.putExtra("productObj",product);
                startActivity(editProductIntent);

            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductDAO productDAO = new ProductDAO(getApplicationContext());
                productDAO.delete(product.getProductId());
                finish();

            }
        });
        }
    }


