package com.example.tanavat.theoffice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewProductActivity extends AppCompatActivity {
    private Button newProductBtn,BACK;
    private EditText productId,productName,productPrice,productdata,productTypeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_product);
        productId = (EditText)findViewById(R.id.product_id);
        productName = (EditText)findViewById(R.id.product_Name);
        productPrice = (EditText)findViewById(R.id.product_price);
        productTypeId = (EditText)findViewById(R.id.type_id);
        productdata = (EditText)findViewById(R.id.product_data);


        BACK = (Button)findViewById(R.id.product_back);
        BACK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SellActivity = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(SellActivity);
            }
        });

        newProductBtn = (Button)findViewById(R.id.product_add_button);
        newProductBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Product product = new Product();
                product.setProductId(String.valueOf(productId.getText()));
                product.setNameProduct(String.valueOf(productName.getText()));
                product.setPrice(Integer.valueOf(String.valueOf(productPrice.getText())));
                product.setTypeId(String.valueOf(productTypeId.getText()));
                product.setData(String.valueOf(productdata.getText()));
                ProductDAO productDAO = new ProductDAO(NewProductActivity.this);
                if (productDAO.add(product)){
                    //  เพิ่มข้อมูล product สำเร็ว
                    Toast.makeText(NewProductActivity.this,"Add new product fail",Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                    Toast.makeText(NewProductActivity.this,"Add new product fail",Toast.LENGTH_SHORT).show();
            }
        });

    }
}
