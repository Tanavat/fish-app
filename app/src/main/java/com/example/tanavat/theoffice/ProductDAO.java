package com.example.tanavat.theoffice;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Tanavat on 3/7/2559.
 */
public class ProductDAO {
    private SQLiteDatabase database;
    private DbHelper dbHelper;

    public ProductDAO(Context context) {
     dbHelper = new DbHelper(context);
     database = dbHelper.getWritableDatabase();
    }
    public  void close(){

        database.close();
    }
    public ArrayList<Product> findAll(){
        ArrayList<Product>products = new ArrayList<>();
        String sqlText = "SELECT * FROM product ORDER BY product_id DESC;";
        Cursor cursor = database.rawQuery(sqlText,null);
        cursor.moveToFirst();
        Product product;
      Product products1;
        while (!cursor.isAfterLast()){
            product = new Product();
            product.setProductId(cursor.getString(0));
            product.setNameProduct(cursor.getString(1));
            product.setPrice(cursor.getInt(2));
            product.setStock(cursor.getInt(3));
            product.setPhoto(cursor.getString(4));
            product.setTypeId(cursor.getString(5));
            product.setData(cursor.getString(6));
            products.add(product);
            cursor.moveToNext();


        }
        return products;

    }

    public boolean add(Product product){
        Product newProduct = product;
        boolean resultNewProduct = false;
        ContentValues values = new ContentValues();
        values.put("product_id",product.getProductId());
        values.put("name_product",product.getNameProduct());
        values.put("price",product.getPrice());
        values.put("stock",product.getStock());
        values.put("type_id",product.getTypeId());
        values.put("data",product.getData());
        this.database.insert("product",null,values);
        Log.d("TheOffice","insert into product");
        return true;
    }

    public void delete(String productId){
        String sqlDelete ="DELETE FROM product WHERE product_id='"+productId+"'";
        this.database.execSQL(sqlDelete);
    }

    public void  edit(Product product){
        Product editProduct = product;
        ContentValues values = new ContentValues();
        values.put("product_id",product.getProductId());
        values.put("name_product",product.getNameProduct());
        values.put("price",product.getPrice());
        values.put("stock",product.getStock());
        values.put("type_id",product.getTypeId());
        values.put("data",product.getData());

        String where = "product_id='"+product.getProductId()+"'";

        this.database.update("product",values,where,null);
        Log.d("TheOffice","Product Updated.");
    }

}

