package com.example.tanavat.theoffice;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Tanavat on 16/7/2559.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final String databaseName = "Fishallappdb.sqlite";
    private static final int databaseVersion = 1;
    private Context myContext;


    public DbHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
        this.myContext = context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        // สร้างตารางเก็บข้อมูลสินค้า
        String SQLText = "CREATE TABLE product (" +
                "product_id TEXT PRIMARY KEY," +
                "name_product TEXT," +
                "price INTEGER," +
                "stock INTEGER," +
                "photo TEXT," +
                "type_id TEXT," +
                "data TEXT);";
        db.execSQL(SQLText);

        // สร้างตารางเก็บข้อมูลชนิดสินค้า
        SQLText = "CREATE TABLE type (" +
                "type_id TEXT PRIMARY KEY," +
                "name_type TEXT);";
        db.execSQL(SQLText);

        //สร้างตารางเก็บข้อมูลการขาย
        SQLText = "CREATE TABLE sale (" +
                "sale_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "customer_id TEXT," +
                "product_id TEXT," +
                "quantity INTEGER," +
                "price INTEGER," +
                "date TEXT);";
        db.execSQL(SQLText);

        //ใส่ข้อมูลสินค้า
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('001','ปลาหางนกยูง พันธุ์คอบร้า',100,10,'Cobra','F5','     มีลวดลายเป็นแถบยาวหรือสั้น พาดขวาง พาดตามยาว หรือ พบพาดเฉียงทั่วลำตัวตลอดถึงโคนหาง ลวดลาย คล้ายลายหนังงู');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('002','ปลาหางนกยูง พันธุ์โมเสค',200,10,'Mosaic','F4','     พื้นลำตัวสีเทาอ่อน บริเวณด้านบนสีฟ้า หรือ เขียว อาจแซมด้วยสีแดง ชมพู หรือ ขาว  ครีบหางมีหลากหลาย');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('003','ปลาหางนกยูง พันธุ์ทักซิโด้',200,10,'Tuxedo','F3','      ลักษณะครึ่งตัวด้านท้ายมีสีดำ หรือ สีน้ำเงินเข้ม  ครีบหางมีหลากหลายแบบ  ครีบหลังและครีบหางหนาใหญ่ มีสีและลวดลายเหมือนกัน');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('004','ปลาหางนกยูง พันธุ์กร๊าซ',100,10,'Grass','F2','      ลำตัวมีหลากสี  ครีบหางมีจุด หรือแต้มเล็ก ๆ กระจาย แผ่ไปทั่งตามรัศมีของหางคล้ายดอกหญ้า ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('005','ปลาหางนกยูง พันธุ์หางดาบ',20,10,'Sword','F1','      ลำตัวมีสีเทา ฟ้า เขียว แดง ชมพู เหลือง คล้ายหางนกยูง พันธุ์พื้นเมือง อาจมีจุด หรือ ลวดลายบนลำตัว');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('006','ปลาทอง พันธุ์ออรันดา',100,10,'Oranda','E6','        มีลักษณะเด่นคือมีวุ้นที่ส่วนหัว (Hood) คล้ายพันธุ์หัวสิงห์  แต่มักไม่ขยายใหญ่เท่าหัวสิงห์  สีของวุ้น');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('007','ปลาทอง พันธุ์ริวกิ้น',50,10,'Ryukin','E5','     เป็นพันธุ์ที่มีหางค่อนข้างยาวเป็นพวงสวยงามเป็นพิเศษ   คล้ายริบบิ้นหรือผ้าแพร');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('008','ปลาทอง พันธุ์ตาโปน',20,10,'Telescope','E4','        ลักษณะเด่นของพันธุ์  คือ  ลูกตาจะยื่นโปนออกมามากเหมือนท่อกล้องส่องทางไกล');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('009','ปลาทอง พันธุ์ลูกโป่ง',20,10,'Bubble','E3','     ลักษณะเด่น  คือ  มีเบ้าตาพองออกคล้ายลูกโป่งทั้งสองข้าง  เวลาว่ายน้ำมักจะแกว่งไปมา   ลำตัวมักมีสีขาวหรือเหลืองแกมส้ม   มีทั้งที่มีครีบหลังและไม่มีครีบหลัง');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('010','ปลาทอง พันธุ์เกล็ดแก้ว',20,10,'Pearl','E2','        ลักษณะลำตัวค่อนข้างกลมคล้ายลูกปิงปอง   ส่วนหัวเล็กมาก หางยาว   ลักษณะเด่น คือ  มีเกล็ดนูนขึ้นมาต่างกับเกล็ดธรรมดาทั่วไปอย่างชัดเจน');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('011','ปลาทอง พันธุ์หัวสิงห์',50,10,'lionhead','E1','      ลักษณะเด่นของพันธุ์นี้คือ   ไม่มีครีบหลัง   หางสั้นและเป็นครีบคู่   ที่สำคัญคือ  ส่วนหัวจะมีก้อนวุ้นปกคลุม');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('012','ปลากัด พันธุ์ป่าภาคกลางเหนือ',50,10,'splendens','D5','      มีลักษณะทางพันธุกรรม ที่สามารถสร้างลักษณะสีและครีบได้มากมายแฝงอยู่ จึงทำให้มีการพัฒนาปลากัด');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('013','ปลากัด พันธุ์ป่าภาคอีสาน',50,10,'imbellis','D4','       ปลากัดที่พบตามแหล่งน้ำธรรมชาติ ในประเทศไทยมีอยู่ ๓ ชนิด แต่มีเพียงชนิดเดียว');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('014','ปลากัด พันธุ์สังกะสี',50,10,'smaragdina','D3','     เป็นปลากัดที่นักเพาะพันธุ์ปลาได้นำมาคัดสายพันธุ์ โดยมุ่งหวังจะได้ปลาที่กัดเก่ง จากบันทึกคำบอกเล่าของหลวงอัมรินทร์สมบัติ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('015','ปลากัด พันธุ์สามเหลี่ยม',50,10,'splenden','D2','        เป็นปลาที่พัฒนาสายพันธุ์มาจากปลากัดครีบยาว หรือปลากัดจีน โดยพัฒนาให้หางสั้นเข้าและแผ่กว้างออกไปเป็นรูปสามเหลี่ยม');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('016','ปลากัด พันธุ์มงกุฎ',100,10,'betta','D1','       เป็นปลากัดที่ได้รับการพัฒนาสายพันธุ์ขึ้นใน พ.ศ. ๒๕๔๓ โดยนักเพาะเลี้ยงปลากัดชาวสิงคโปร์ เป็นปลากัดสายพันธุ์ใหม่ ที่มีหางจักเป็นหนาม เหมือนมงกุฎ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo,type_id , data)" +
                "VALUES ('017','ปลาหมอสี พันธุ์หัวใหญ่',1200,10,'bighead','C5','        เป็นปลาหมอสีครอสบรีดอีกชนิดหนึ่ง ซึ่งได้ถือกำเนิดขึ้นจาก ฝีมือนักเพาะพันธุ์ปลาหมอสีชาวไทยเมื่อประมาณ 6-7 ปีที่แล้ว ซินแดง เป็นปลาหมอสีที่นำมาข้ามสายพันธุ์');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('018','ปลาหมอสี พันธุ์เคลือบสี',1500,10,'bf','C4','        แดงเกิดจากการนำ เท็กซัสเขียว มาผสมข้ามสายพันธุ์กับปลาเพศเมียที่มีลักษณะสีแดงเข้ม อาทิ นกแก้ว, คิงคอง, ซินแดง, ไตรทอง, เรดเดวิล, ซุปเปอร์เรดซิน');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('019','ปลาหมอสี พันธุ์หัวแดงจัด',1200,10,'superred','C3','     ลักษณะความสวยงามโดดเด่นเฉพาะตัว คือ ผิวปลามีเนื้อสีเหลืองทอง เรียบเนียน ลักษณะเกล็ดเรียงกันเป็นระเบียบ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('020','ปลาหมอสี พันธุ์ครอสบรีด',700,10,'superred1','C2','      ปลาหมอสีครอสบรีดอีกชนิดหนึ่ง ที่ได้เคยสร้างชื่อเสียงให้กับประเทศไทย และเป็นความภาคภูมิใจอันยิ่งอันใหญ่ของคนไทย ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('021','ปลาหมอสี พันธุ์ลำตัวสั้น',500,10,'shortbody','C1','     สายพันธุ์ จนเป็นที่ยอมรับและเป็นที่นิยมของคนในประเทศ ทำให้ตลาดของปลาหลอฮั่นนั้นเติบโตอย่างรวดเร็ว จนเป็นที่สนใจของตลาดต่างประเทศ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('022','ปลาคาร์พ พันธุ์คาวาริโมโนะ',2000,10,'Kawarimono','B12','        ปลาที่มีสีดำกลางเกล็ด หากผิวมันจะถูกจัดอยู่ในกลุ่มฮิคาริ หากผิวไม่มัน อยู่ในกลุ่มคาวาริ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('023','ปลาคาร์พ พันธุ์ฮิการิอุจึริโมโนะ',1800,10,'hikariutsur','B11','     ปลาไม่มีเกล็ด ต้นกำเนิดมาจากเยอรมัน');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('024','ปลาคาร์พ พันธุ์ฮิการิโมโยโมโนะ',3200,10,'Hikarimoyomono','B10','        ปลาที่มีแต้มดำบนลายโคฮากุ เพราะเกิดในยุคไทโชของญี่ปุ่น จึงเรียกว่าไทโชซันโชกุ เรียกสั้นๆว่าไทโชซังเก้ หรือซังเก้');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('025','ปลาคาร์พ สายพันธุ์ฮิการิ',1500,10,'Hikari','B9','       ปลาที่มีสีแดงเฉพาะเป็นดวงกลมตรงหัวของปลา ลายที่เหลือเป็นไปตามสายพันธุ์เช่น Tancho Kohaku, Doitsu Tancho Kohaku, Tancho Showa, Tancho Goshiki เป็นต้น');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('026','ปลาคาร์พ พันธุ์โงชิกิ',5200,10,'Goshiki','B8','     ทั้งตัวมีสีฟ้าหรือสีคราม รอยต่อระหว่างเกล็ดลายเหมือนตาข่าย เป็นปลาดั้งเดิมของนิชิกิกอย');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('027','ปลาคาร์พ พันธุ์โกโระโมะ',1500,10,'Koromo','B7','        แปลตรงตัวว่า 5 สี มีตาข่ายสีฟ้าบนลายโคฮากุ บางตัวมีตาข่ายเฉพาะส่วนที่เป็นสีขาว สีแดงและสีฟ้าที่เหลื่อมกันทำให้เกิดเป็นสีม่วงส่วนมากตรงบริเวณหัว เกล็ดไม่มันวาว ซึ่งทำให้ต่างจาก Kujyaku');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('028','ปลาคาร์พ พันธุ์ซูซุย',2000,10,'Shusui','B6','       ปลาที่มีสีดำกลางเกล็ด หากผิวมันจะถูกจัดอยู่ในกลุ่มฮิคาริ หากผิวไม่มัน อยู่ในกลุ่มคาวาริ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('029','ปลาคาร์พ พันธุ์อาซากิ',2100,10,'Asagi','B5','       ปลาที่มีเกล็ดแวววาวระยิบระยับเหมือนโรยด้วยกากเพชร');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('030','ปลาคาร์พ พันธุ์อุจึริโมโนะ',1100,10,'Utsurimono','B4','     แปลตรงตัวว่า 5 สี มีตาข่ายสีฟ้าบนลายโคฮากุ บางตัวมีตาข่ายเฉพาะส่วนที่เป็นสีขาว สีแดงและสีฟ้าที่เหลื่อมกันทำให้เกิดเป็นสีม่วงส่วนมากตรงบริเวณหัว เกล็ดไม่มันวาว ซึ่งทำให้ต่างจาก Kujyaku');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('031','ปลาคาร์พ พันธุ์เบคโกะ',2000,10,'Bekko','B3','       ปลาที่มีลายเส้นสีดำบนสีพื้นสีเดียว เช่น Shiro Utsuri (ขาว) Hi Utsuri (แดง) Ki Utsuri (เหลือง)');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('032','ปลาคาร์พ พันธุ์โชวาซันโชกุ',1000,10,'Showa','B2','      ปลาที่มีลายเป็นเส้นดำบนลายโคฮากุ เกิดในยุคโชว่าจึงเรียกว่าโชว่าซันโชกุ หรือโชว่า');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('033','ปลาคาร์พ พันธุ์ไทโชซันโชกุ',500,10,'Taisho','B1','      ปลาที่มีลายแดงบนพื้นขาว กล่าวกันว่าคนเลี้ยงปลาคาร์พมักเริ่มต้นด้วยโคฮากุและลงท้ายที่โคฮากุ โคฮากุ ไทโชซันโชกุ และโชว่าซันโชกุ เป็นสายพันธุ์ที่คนนิยมที่สุด เรียกกันว่า Gosanke Big 3');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('034','ปลาอะโรวาน่า พันธุ์เหลือง',1500,10,'Yelo','A7','        มีลักษณะคล้ายคลึงมังกรเขียวมากที่สุด แต่ในความเป็นจริง ระดับชั้นของ Banjar Red ทั้งหมดนั้น หน้าตาจะคล้ายคลึงกับมังกรเขียว และทองอ่อนมาก โดยเฉพาะสีเกล็ด และสีของลำตัว จะต่างกันก็ตรงที่สีหาง');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('035','ปลาอะโรวาน่า พันธุ์ดำ',1000,10,'Back','A6','        ลักษณะลำตัวโดยทั่วไปจะคล้ายคลึงกัน กับอะโรวาน่าเงินมากในขณะที่ปลาอายุยังน้อยยังมีเส้นขนาดเล็กคาดอยู่ อะโรวาน่าดำจะมีสีคล้ำกว่าอะโรวาน่าเงินมาก');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('036','ปลาอะโรวาน่า พันธุ์อินโดนีเซีย',15000,10,'Golden','A5','        สายพันธุ์นี้ ตามแหล่งที่มาของปลา ปลาสายพันธุ์นี้เกล็ด บนลำตัวจะมีสีทองอร่าม บริเวณแผ่นหลัง จะออกสีน้ำตาล แกมดำ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('037','ปลาอโรวาน่า พันธุ์ซุปเปอร์เรด',20000,10,'superred','A4','       สายพันธุ์นี้ เกล็ดบนลำตัว จะออกสีส้มอมทอง หรือ สีส้มอมเขียว บางตัวที่มีสีเข้มหน่อย ก็ออกสีทองอมแดง');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('038','ปลาอะโรวาน่า พันธุ์ทองมาเลเซีย',3200,10,'Xback','A3','      สายพันธุ์นี้ ตามแหล่งที่มาของปลา ปลาสายพันธุ์นี้เกล็ด มีสีสันใกล้เคียง กับปลาอะโรวาน่า ทองอินโดนีเซียมากที่สุด');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('039','ปลาอะโรวาน่า พันธุ์เขียว',2000,10,'Green','A2','    โดยปกติปลา อะโรวาน่า สายพันธุ์สีเขียว จะมีลำตัว เป็นพื้นสีเงิน จึงมีชื่อว่า Silver และบางแหล่งก็เรียกว่า Platinum แต่ที่นิยมเรียก');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (" +
                "product_id , name_product ," +
                "price , stock , photo , type_id , data)" +
                "VALUES ('040','ปลาอะโรวาน่า พันธุ์เงิน',700,10,'Silver','A1','     เป็นอะโรวาน่า ที่ผู้คิดจะเลี้ยงปลา ในกลุ่มนี้ ควรเริ่มต้นเลี้ยงมากที่สุด เพราะราคาไม่แพงนัก สวยทั้งรูปร่าง สีสัน สง่าในการว่าย');";
        db.execSQL(SQLText);


    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
    }
}



