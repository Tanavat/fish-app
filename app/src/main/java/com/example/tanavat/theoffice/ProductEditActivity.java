package com.example.tanavat.theoffice;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class ProductEditActivity extends AppCompatActivity {
    private EditText productName,productPrice,productdata,productTypeId;
    private Button editBtn,BACK ;
    private Product product;
    private ImageView productImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_edit);
        final Intent intent =getIntent();
        product = (Product) intent.getSerializableExtra("productObj");

        productName = (EditText)findViewById(R.id.product_Name);
        productPrice = (EditText)findViewById(R.id.product_price);
        productdata = (EditText)findViewById(R.id.product_data);
        productTypeId = (EditText)findViewById(R.id.type_id);

        productName.setText(product.getNameProduct());
        productPrice.setText(String.valueOf(product.getPrice()));
        productdata.setText(String.valueOf(product.getData()));
        productTypeId.setText(product.getTypeId());

        BACK = (Button)findViewById(R.id.product_back);
        BACK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent SellActivity = new Intent(getApplicationContext(),SellActivity.class);
//                startActivity(SellActivity);
                Intent SellActivity = new Intent(getApplicationContext(),SellActivity.class);
                startActivity(SellActivity);
            }
        });

        editBtn = (Button)findViewById(R.id.product_edit_button);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductDAO productDAO = new ProductDAO(getApplicationContext());
                Product editProduct = new Product();
                editProduct.setProductId(product.getProductId());
                editProduct.setNameProduct(String.valueOf(productName.getText()));
                editProduct.setPrice(Integer.valueOf(String.valueOf(productPrice.getText())));
                editProduct.setData(String.valueOf(productdata.getText()));
                editProduct.setTypeId(String.valueOf(productTypeId.getText()));



                productDAO.edit(editProduct);
                Toast.makeText(getApplicationContext(),"Product Edit Complete",Toast.LENGTH_SHORT).show();

            }
        });

        productImage = (ImageView)findViewById(R.id.product_image);
        productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent,1);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((resultCode==RESULT_OK)&&(requestCode==1)){
            Uri uri = data.getData();
            //   productImage.setImageURI(uri);



        }
    }

}
