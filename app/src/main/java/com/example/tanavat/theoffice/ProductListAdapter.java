package com.example.tanavat.theoffice;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tanavat on 3/7/2559.
 */
public class ProductListAdapter extends BaseAdapter {
    private static Activity activity;
    private static LayoutInflater layoutInflater;
    private ArrayList<Product> products;
    private  Integer[] imgFish = {
            R.drawable.a1 ,
            R.drawable.a2 ,
            R.drawable.a3,
            R.drawable.a4
    };

    public ProductListAdapter(Activity activity,ArrayList<Product> product) {
        this.activity = activity;
        this.products = product;
        this.layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.imgFish = imgFish;

    }



    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Product getItem(int position) {
        return this.products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(this.products.get(position).getProductId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        view = layoutInflater.inflate(R.layout.listview_product,null);
        ImageView  imgFish  = (ImageView)view.findViewById(R.id.img_product);
        TextView nameProduct = (TextView) view.findViewById(R.id.name_product);
        TextView typeId = (TextView)view.findViewById(R.id.type_id);

        nameProduct.setText(this.products.get(position).getNameProduct());
        typeId.setText(String.valueOf(this.products.get(position).getTypeId()));
//       imgFish.setImageResource(this.imgFish[position]);

        return view;
    }
}
