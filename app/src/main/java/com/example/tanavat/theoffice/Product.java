package com.example.tanavat.theoffice;

import java.io.Serializable;

/**
 * Created by Tanavat on 26/6/2559.
 */
public class Product  implements Serializable{
    private String productId;
    private String nameProduct;
    private int Price;
    private int stock;
    private String photo;
    private String typeId;
    private String data;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int Price) {
        this.Price = Price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getData() {return data;}

    public void setData(String data) {this.data =data;}
}



